namespace MyExcel.ViewModels
{
    using System.Collections.ObjectModel;
    using System.ComponentModel;

    public class RowViewModel : ObservableCollection<CellViewModel>
    {
        private int _number;

        public RowViewModel(int number)
        {
            Number = number;
        }

        public int Number
        {
            get { return this._number; }
            private set
            {
                this._number = value;
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(Number)));
            }
        }
    }
}