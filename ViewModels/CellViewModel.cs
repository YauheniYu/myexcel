﻿namespace MyExcel.ViewModels
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using Annotations;
    using Models;

    public class CellViewModel : INotifyPropertyChanged
    {
        private readonly Cell _cell;

        private bool _isInputMode;

        private bool _isShowExpression;

        public CellViewModel(Cell cell)
        {
            this._cell = cell;
            this._cell.Changed += _cell_Changed;
        }

        private void _cell_Changed(object sender, System.EventArgs e)
        {
            OnPropertyChanged(nameof(Value));
        }

        public string Value
        {
            get
            {
                this._isShowExpression = this._isInputMode;
                return this._isShowExpression ? this._cell.Expression.Value : this._cell.Value.ToString();
            }
            set
            {
                this._cell.Expression.Value = value;
                this._isShowExpression = false;
                OnPropertyChanged(nameof(Value));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void OffInputMode(bool isCommit)
        {
            if (!isCommit)
            {
                this._isShowExpression = false;
                OnPropertyChanged(nameof(Value));
            }
            this._isInputMode = false;
        }

        public void OnInputMode()
        {
            this._isInputMode = true;
            this._isShowExpression = true;
            OnPropertyChanged(nameof(Value));
        }
    }
}