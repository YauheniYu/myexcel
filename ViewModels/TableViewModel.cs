namespace MyExcel.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using Command;
    using Infrastructure;
    using Models;

    public class TableViewModel : ObservableCollection<RowViewModel>
    {
        private readonly Table _table;

        public TableViewModel(int rows, int columns)
        {
            this._table = new Table(rows, columns);
          
            for (var i = 0; i < columns; i++)
            {
                ColumnHeaders.Add(new DataGridTextColumn
                                  {
                                      Header = TableInfrastructure.GetColumnName(i),
                                      Binding = new Binding($"[{i}].Value")
                                                {
                                                    Mode = BindingMode.TwoWay,
                                                    UpdateSourceTrigger = UpdateSourceTrigger.LostFocus
                                                },
                                      CanUserSort = false,
                                      CanUserReorder = false
                                  });
            }

            for (var i = 0; i < rows; i++)
            {
                var row = new RowViewModel(i + 1);
                for (var j = 0; j < columns; j++)
                {
                    row.Add(new CellViewModel(this._table[i, j]));
                }

                Add(row);
            }
        }

        public ObservableCollection<DataGridColumn> ColumnHeaders { get; set; } =
            new ObservableCollection<DataGridColumn>();

        #region OnInputModeCommand

        private DelegateCommand<object> _onInputModeCommand;

        public ICommand OnInputModeCommand => this._onInputModeCommand ??
                                              (this._onInputModeCommand =
                                               new DelegateCommand<object>(OnInputMode, CanOnInputMode));

        private void OnInputMode(object arg)
        {
            var eventArg = arg as DataGridBeginningEditEventArgs;
            var cell = ((RowViewModel)eventArg.Row.Item)[eventArg.Column.DisplayIndex];
            cell.OnInputMode();
        }

        private bool CanOnInputMode(object arg)
        {
            var eventArg = arg as DataGridBeginningEditEventArgs;
            return eventArg != null;
        }

        #endregion

        #region OffInputModeCommand

        private DelegateCommand<object> _offInputModeCommand;

        public ICommand OffInputModeCommand => this._offInputModeCommand ??
                                               (this._offInputModeCommand =
                                                new DelegateCommand<object>(OffInputMode, CanOffInputMode));

        private void OffInputMode(object arg)
        {
            var eventArg = arg as DataGridCellEditEndingEventArgs;
            var cell = ((RowViewModel)eventArg.Row.Item)[eventArg.Column.DisplayIndex];
            cell.OffInputMode(eventArg.EditAction == DataGridEditAction.Commit);
        }

        private bool CanOffInputMode(object arg)
        {
            var eventArg = arg as DataGridCellEditEndingEventArgs;
            return eventArg != null;
        }

        #endregion
    }
}