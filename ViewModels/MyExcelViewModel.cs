namespace MyExcel.ViewModels
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows.Controls;
    using System.Windows.Input;
    using Annotations;
    using Command;

    public class MyExcelViewModel : INotifyPropertyChanged
    {
        private int _columnCount;

        private DataGridCellInfo _currentCellInfo;

        private int _rowCount;

        public int RowCount
        {
            get { return this._rowCount; }
            set
            {
                this._rowCount = Math.Abs(value);
                OnPropertyChanged(nameof(RowCount));
            }
        }

        public int ColumnCount
        {
            get { return this._columnCount; }
            set
            {
                this._columnCount = Math.Abs(value);
                OnPropertyChanged(nameof(ColumnCount));
            }
        }

        public CellViewModel CurrentCell => 
            CurrentCellInfo.Column != null
            ? ((RowViewModel)CurrentCellInfo.Item)[CurrentCellInfo.Column.DisplayIndex]
            : null;

        public DataGridCellInfo CurrentCellInfo
        {
            get { return this._currentCellInfo; }
            set
            {
                this._currentCellInfo = value;
                OnPropertyChanged(nameof(CurrentCellInfo));
                OnPropertyChanged(nameof(CurrentCell));
            }
        }

        public TableViewModel Table { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region NewTableCommand

        private DelegateCommand _newTableCommand;

        public ICommand NewTableCommand => this._newTableCommand ??
                                           (this._newTableCommand =
                                            new DelegateCommand(NewTable, () => !(RowCount == 0 || ColumnCount == 0)));

        private void NewTable()
        {
            Table = new TableViewModel(RowCount, ColumnCount);

            OnPropertyChanged(nameof(Table));
        }

        #endregion
    }
}