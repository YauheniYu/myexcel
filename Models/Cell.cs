namespace MyExcel.Models
{
    using System;

    public class Cell
    {
        public Cell(string name, Expression expression)
        {
            Name = name;
            Expression = expression;
            Expression.Changed += Expression_Changed;
        }

        public string Name { get; private set; }

        public object Value => Expression.Calculate();

        public Expression Expression { get; }

        private void Expression_Changed(object sender, EventArgs e) { OnChanged(); }

        public event EventHandler Changed;

        protected virtual void OnChanged() { Changed?.Invoke(this, EventArgs.Empty); }

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}