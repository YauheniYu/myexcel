namespace MyExcel.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class Expression
    {
        private readonly Table _source;

        private string _error = string.Empty;

        private List<string> _operators = new List<string>();

        private List<object> _arguments = new List<object>();

        private string _value = string.Empty;

        public Expression(Table source) { this._source = source; }

        /// <summary>
        /// expression to calculate the value of the cell
        /// </summary>
        public string Value
        {
            get { return this._value; }
            set
            {
                this._value = value;
                Parse(this._value);
                OnChanged();
            }
        }

        private void Clear()
        {
            this._operators.Clear();
            this._arguments.Clear();
        }

        /// <summary>
        /// It converts a string expression to the lists of operators and arguments
        /// </summary>
        /// <param name="expression"></param>
        private void Parse(string expression)
        {
            Clear();
            this._error = string.Empty;

            if (string.IsNullOrWhiteSpace(expression))
            {
                this._arguments.Add(string.Empty);
                return;
            }

            switch (expression[0])
            {
                case '=':
                    ArithmeticExpressionParse(expression);
                    break;
                case '\'':
                    this._arguments.Add(expression.Remove(0, 1));
                    break;
                default:
                    double val;

                    if (double.TryParse(expression, out val))
                    {
                        this._arguments.Add(val);
                        break;
                    }

                    this._arguments.Add(expression);
                    break;
            }
        }

        private void ArithmeticExpressionParse(string expression)
        {
            try
            {
                var arguments =
                expression.Remove(0, 1).Split(new[] { "+", "-", "*", "/" }, StringSplitOptions.None).ToList();
                if (arguments.Any(string.IsNullOrWhiteSpace))
                {
                    throw new Exception("There is no argument between operators");
                }

                AddArguments(arguments);

                this._operators =
                expression.Remove(0, 1).Split(arguments.ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

                if (this._operators.Count != this._arguments.Count - 1)
                {
                    throw new Exception("Incomplete expression");
                }

            }
            catch (Exception ex)
            {
                Clear();
                this._error = ex.Message;
            }
        }

        private void AddArguments(IEnumerable<string> arguments)
        {
            foreach (var argument in arguments)
            {
                double value;
                if (double.TryParse(argument, out value))
                {
                    this._arguments.Add(value);
                }
                else
                {
                    var cell = this._source[argument.Trim()];
                    CheckCell(cell);
                    cell.Changed += Cell_Changed;
                    this._arguments.Add(cell);
                }
            }
        }

        private static void CheckCell(Cell cell)
        {
            if (cell == null || !string.IsNullOrEmpty(cell.Expression.Value) && !(cell.Value is double))
            {
                throw new Exception("Argument Error");
            }
        }

        private void Cell_Changed(object sender, EventArgs e)
        {
            try
            {
                CheckCell(sender as Cell);
                if (!string.IsNullOrEmpty(this._error))
                {
                    Parse(Value);
                }
            }
            catch (Exception ex)
            {
                _error = ex.Message;
            }
            finally
            {
                OnChanged();
            }
        }

        public object Calculate()
        {
            try
            {
                if (!string.IsNullOrEmpty(this._error))
                {
                    throw new Exception(_error);
                }

                if (string.IsNullOrEmpty(Value))
                {
                    return string.Empty;
                }

                if (_arguments.Count == 1 && _arguments[0] is string)
                {
                    return _arguments[0];
                }

                var result = GetArgumentValue(0);
                var i = 0;
                foreach (var operation in this._operators)
                {
                    i++;
                    OperationCalculate(ref result, operation, GetArgumentValue(i));
                }

                return result;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }

        private static void OperationCalculate(ref double firstArg, string operation, double secondArg)
        {
                switch (operation)
                {
                    case "+":
                        firstArg += secondArg;
                        break;
                    case "-":
                        firstArg -= secondArg;
                        break;
                    case "*":
                        firstArg *= secondArg;
                        break;
                    case "/":
                        firstArg /= secondArg;
                        break;
                    default:
                        throw new Exception("unknow operator");
                }
        }

        private double GetArgumentValue(int index)
        {
            if (this._arguments[index] is double)
            {
                return (double) this._arguments[index];
            }

            if (this._arguments[index].GetType() == typeof (Cell))
            {
                var cell = this._arguments[index] as Cell;

                if (cell!=null)
                {
                    return string.IsNullOrEmpty(cell.Expression.Value) ? 0 : (double)cell.Value;
                }
                
            }

            throw new ArgumentException("unknown data format");
        }

        public event EventHandler Changed;

        protected virtual void OnChanged() { Changed?.Invoke(this, EventArgs.Empty); }

        public override string ToString()
        {
            return Value;
        }
    }
}