namespace MyExcel.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Infrastructure;

    public class Table
    {
        private readonly List<Cell> _cells = new List<Cell>();

        public Table(int rows, int columns)
        {
            for (var i = 0; i < rows; i++)
            {
                for (var j = 0; j < columns; j++)
                {
                    var cell = new Cell(TableInfrastructure.GetName(i, j), new Expression(this));
                    cell.Changed += Cell_Changed;
                    this._cells.Add(cell);
                }
            }
        }

        public Cell this[string cellName] { get { return this._cells.SingleOrDefault(c => c.Name == cellName); } }

        public Cell this[int i, int j]
        {
            get
            {
                var name = TableInfrastructure.GetName(i, j);
                return this[name];
            }
        }

        public void AutoComplete()
        {
            foreach (var cell in this._cells)
            {
                cell.Expression.Value = cell.Name;
            }
        }
        private void Cell_Changed(object sender, EventArgs e) { OnChanged(); }

        public event EventHandler Changed;

        protected virtual void OnChanged() { Changed?.Invoke(this, EventArgs.Empty); }
    }
}