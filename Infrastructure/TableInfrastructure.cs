﻿namespace MyExcel.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class TableInfrastructure
    {
        /// <summary>
        /// Specifies the name of cell for its position in the table
        /// </summary>
        /// <param name="row">row number of cell</param>
        /// <param name="column">column number of cell</param>
        /// <returns></returns>
        public static string GetName(int row, int column) { return $"{GetColumnName(column)}{row + 1}"; }

        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// converts column number to a string. Ex.: number '26' to string "AA"
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public static string GetColumnName(int column)
        {
            var word = new List<char>();
            const int mod = 26;
            do
            {
                word.Add(Convert.ToChar('A' + column%mod));
                column = column/mod - 1;
            } while (column != -1);

            word.Reverse();
            var result = new StringBuilder();
            result.Append(word.ToArray());
            return result.ToString();
        }
    }
}